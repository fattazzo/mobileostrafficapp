package it.mobileos.fattazzo.app;

import it.mobileos.fattazzo.app.component.TrafficGraphView;
import android.app.Activity;
import android.app.AlertDialog;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;

/**
 * 
 * @author fattazzo - fattazzo82 [at] gmail . com
 * 
 */
public class MainActivity extends Activity implements OnItemSelectedListener {

	private Handler trafficHandler = new Handler();

	private int tick = 0;

	private long startingRXByte = 0;
	private long startingRXPackets = 0;

	private long startingTXByte = 0;
	private long startingTXPackets = 0;

	private GraphView graphView = null;
	private SparseArray<GraphViewSeries[]> mapSeries = null;

	public long getStartingRXByte() {
		return startingRXByte;
	}

	public long getStartingRXPackets() {
		return startingRXPackets;
	}

	public long getStartingTXByte() {
		return startingTXByte;
	}

	public long getStartingTXPackets() {
		return startingTXPackets;
	}

	public Handler getTrafficHandler() {
		return trafficHandler;
	}

	/**
	 * Inizializzazione del grafico e delle serie.
	 */
	private void initGraphs() {

		// creo le serie
		mapSeries = new SparseArray<GraphViewSeries[]>();
		mapSeries.put(0, TrafficGraphView.createSeries());
		mapSeries.put(1, TrafficGraphView.createSeries());
		mapSeries.put(2, TrafficGraphView.createSeries());
		mapSeries.put(3, TrafficGraphView.createSeries());

		graphView = new TrafficGraphView(this);
		graphView.addSeries(mapSeries.get(0)[0]);
		graphView.addSeries(mapSeries.get(0)[1]);
		// aggiungo il grafico al frame
		FrameLayout graphLayout = (FrameLayout) findViewById(R.id.frameGraph);
		graphLayout.addView(graphView);
	}

	/**
	 * Inizializzazione dello spinner di selezione.
	 */
	private void initSpinnerGraphType() {
		// ottengo il controllo che visualizza la scelta del tipo di grafico
		Spinner spinner = (Spinner) findViewById(R.id.spinnerGraphType);

		// creo l'ArrayAdapter che contiene tutte le scelte possibili che vengono caricare dall'array
		// definito nel file strings.xml
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.graph_type,
				android.R.layout.simple_spinner_item);
		// Vado a definire che l'adapter si presenterà in forma di drop down list
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Applico l'adapter al controllo
		spinner.setAdapter(adapter);

		// Aggiungo il listener che si attiva sul cambio di selezione
		spinner.setOnItemSelectedListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// inizializzo il grafico e le serie
		initGraphs();

		// inizializzo lo spinner per la scelta del tipo di grafico
		initSpinnerGraphType();

		// salvo i valori di partenza di bytes e pacchetti trasmessi e ricevuti
		startingRXByte = TrafficStats.getTotalRxBytes();
		startingRXPackets = TrafficStats.getTotalRxPackets();
		startingTXByte = TrafficStats.getTotalTxBytes();
		startingTXPackets = TrafficStats.getTotalTxPackets();

		// se il dispositivo non supporta il monitoraggio del traffico visualizzo il messaggio e mi fermo
		if (startingRXByte == TrafficStats.UNSUPPORTED || startingTXByte == TrafficStats.UNSUPPORTED) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("ATTENZIONE");
			alert.setMessage("Il device non supporta il monitoraggio del traffico.");
			alert.show();
		} else {
			trafficHandler.postDelayed(new UpdateTrafficaRunnable(this), 1000);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long arg3) {

		// al cambio del tipo di grafico rimuovo le serie esistenti e le sostituisco con quelle appropriate
		graphView.removeAllSeries();
		graphView.addSeries(mapSeries.get(pos)[0]);
		graphView.addSeries(mapSeries.get(pos)[1]);

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public void setTick(int tick) {
		this.tick = tick;
	}

	public void updateGraph(long rxOverallBytes, long txOverallBytes, long rxOverallPackets, long txOverallPackets,
			long rxLiveBytes, long txLiveBytes, long rxLivePackets, long txLivePackets) {

		tick++;

		updateSeries(mapSeries.get(0)[0], rxLiveBytes);
		updateSeries(mapSeries.get(0)[1], txLiveBytes);

		updateSeries(mapSeries.get(1)[0], rxLivePackets);
		updateSeries(mapSeries.get(1)[1], txLivePackets);

		updateSeries(mapSeries.get(2)[0], rxOverallBytes);
		updateSeries(mapSeries.get(2)[1], txOverallBytes);

		updateSeries(mapSeries.get(3)[0], rxOverallPackets);
		updateSeries(mapSeries.get(3)[1], txOverallPackets);

		graphView.redrawAll();
	}

	private void updateSeries(GraphViewSeries serie, long value) {
		serie.appendData(new GraphViewData(tick, value), true, TrafficGraphView.GRAPH_MAX_DATA);
	}

}
