package it.mobileos.fattazzo.app.component;

import android.content.Context;
import android.graphics.Color;

import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;

/**
 * 
 * @author fattazzo - fattazzo82 [at] gmail . com
 * 
 */
public class TrafficGraphView extends LineGraphView {

	public static final int GRAPH_MAX_DATA = 30;

	/**
	 * Crea 2 serie per il grafico, la prima per i dati ricevuti, la seconda per i dati trasmessi.
	 * 
	 * @return serie create
	 */
	public static GraphViewSeries[] createSeries() {
		return new GraphViewSeries[] {
				new GraphViewSeries("Ricevuti", new GraphViewSeriesStyle(Color.RED, 3),
						new GraphViewData[] { new GraphViewData(0, 0) }),
				new GraphViewSeries("Trasmessi", new GraphViewSeriesStyle(Color.GREEN, 3),
						new GraphViewData[] { new GraphViewData(0, 0) }) };
	}

	public TrafficGraphView(Context context) {
		super(context, "");
		setViewPort(0, GRAPH_MAX_DATA);
		setScrollable(true);
		setScalable(true);
	}
}
